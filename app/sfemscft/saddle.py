#!/usr/bin/env python3
#

import sys
import numpy as np
import mpl_toolkits.mplot3d as a3
import pylab as pl
import pickle

from gensurface_problem import saddle_model
from SSCFTFEMModel import sscftmodel_options
from fealpy.opt.saddleoptalg import SteepestDescentAlg

moptions = sscftmodel_options(
        nspecies= 2, 
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.2,
        chiAB = 0.25,
        dim = 2,
        p = 2,
        T0 = 40,
        T1 = 160,
        nupdate = 1,
        integrator=6,
        scale = 6.0,
        outputdata=sys.argv[1],
        outputfig=True)


problem = saddle_model(refines=0, fieldstype=3, options = moptions)

options = {
        'MaxIters': 500,
        'MaxFunEvals': 5000,
        'NormGradTol': 1e-4,
        'FunValDiff': 1e-6,
        'StepLength': 2,
        'StepTol': 1e-14,
        'Output': True
        }

optalg = SteepestDescentAlg(problem, options)
optalg.run()

model.save_data(fname=str(int(moptions['chiN']))+'.mat', filename=str(int(moptions['chiN']))+'.txt')

fig = pl.figure()
axes = a3.Axes3D(fig)
mesh.add_plot(axes, linewidths=0.5)
pl.show()
