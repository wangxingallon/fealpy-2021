import numpy as np
from SSCFTFEMModel import SSCFTFEMModel, sscftmodel_options
from fealpy.mesh import Tritree
from fealpy.mesh import TriangleMesh, LagrangeTriangleMesh
import scipy.io as sio

__doc__="""
该文件包含了所有用来测试的问题模型
"""


def sphere_model(radius=3.56, refines=5, fieldstype=2, options=None):
    """
    构造球面上的模型
    """
    from fealpy.geometry import SphereSurface
    surface = SphereSurface()
    surface.radius = radius
    mesh = surface.init_mesh(p=options['p'])
    mesh.uniform_refine(n=refines)
    
    #spheredata = sio.loadmat('results/25spheredata37.mat')
    #mu = spheredata['mu']

    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)
    problem = {
            'objective': obj,
            'x0': mu,
        }
    return problem

def sphere_adaptive_model(radius=3.56, refines=4, fieldstype=2, options=None):
    """
    构造球面上的自适应模型
    """
    from fealpy.geometry import SphereSurface
    surface = SphereSurface()
    surface.radius = radius
    mesh = surface.init_mesh(p=options['p'])
    mesh.uniform_refine(n=refines)
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    #tritree.uniform_refine(n=refines, options=options, surface=surface)
    #mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)          
    problem = {
            'objective': obj,
            'x0': mu,
            'tritree': tritree,
            'surface': surface
        }
    return problem

def converge_model(options=None):
    """
    构造收敛的自适应模型
    """
    spheredata = sio.loadmat('25.mat')
    node = spheredata['node']
    cell = spheredata['elem'] - 1
    mu = spheredata['mu']
    rho = spheredata['rho']

    import pickle
    mysurface = open('25surface', 'rb')
    surface = pickle.load(mysurface)
    mysurface.close()

    myfile = open('25tritree', 'rb')
    tritree = pickle.load(myfile)
    myfile.close()

    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {
            'objective': obj,
            'x0': mu,
            'tritree': tritree,
            'surface': surface,
            'rho': rho
        }
    return problem

