import numpy as np
from SSCFTFEMModel import SSCFTFEMModel, sscftmodel_options
from fealpy.mesh import Tritree
from fealpy.mesh import TriangleMesh
import scipy.io as sio


def heart_model(refines=0, fieldstype=2, options=None):
    """
    构造心型曲面上的模型
    """
    from fealpy.geometry import HeartSurface
    surface = HeartSurface()
    mesh = surface.init_mesh('meshdata/heart.mat')
    mesh.uniform_refine(n=refines, surface=surface)
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)
    problem = {
            'objective': obj,
            'x0': mu,
        }
    return problem


def heart_adaptive_model(refines=0, options=None):
    """
    构造心型曲面上的自适应模型
    """
    spheredata = sio.loadmat('init.mat')
    mu = spheredata['mu']

    from fealpy.geometry import HeartSurface

    surface = HeartSurface()
    mesh = surface.init_mesh('meshdata/heart.mat')
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    tritree.uniform_refine(n=refines, options=options, surface=surface)
    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {'objective': obj, 
               'x0': mu,
               'tritree': tritree,
               'surface': surface}
    return problem


def parabolic_model(refines=0, fieldstype=2, options=None):
    """
    构造抛物曲面上的模型
    """
    from fealpy.geometry import ParabolicSurface
    surface = ParabolicSurface()
    mesh = surface.init_mesh('meshdata/parabolic.mat')
    mesh.uniform_refine(n=refines, surface=surface)
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)
    problem = {
            'objective': obj,
            'x0': mu,
        }
    return problem


def parabolic_adaptive_model(refines=0, options=None):
    """
    构造抛物曲面上的自适应模型
    """
    spheredata = sio.loadmat('init.mat')
    mu = spheredata['mu']

    from fealpy.geometry import ParabolicSurface

    surface = ParabolicSurface()
    mesh = surface.init_mesh('meshdata/parabolic.mat')
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    tritree.uniform_refine(n=refines, options=options, surface=surface)
    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {'objective': obj, 
               'x0': mu,
               'tritree': tritree,
               'surface': surface}
    return problem


def quartics_model(refines=0, fieldstype=2, options=None):
    """
    构造四次曲面上的模型
    """
    from fealpy.geometry import QuarticsSurface

    surface = QuarticsSurface()
    mesh = surface.init_mesh('meshdata/quartics.mat')
    mesh.uniform_refine(n=refines, surface=surface)
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)              
    problem = {'objective': obj, 
               'x0': mu}
    return problem


def quartics_adaptive_model(refines=0, options=None):
    """
    构造四次曲面上的自适应模型
    """
    spheredata = sio.loadmat('init.mat')
    mu = spheredata['mu']

    from fealpy.geometry import QuarticsSurface

    surface = QuarticsSurface()
    mesh = surface.init_mesh('meshdata/quartics.mat')
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    tritree.uniform_refine(n=refines, options=options, surface=surface)
    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {'objective': obj, 
               'x0': mu,
               'tritree': tritree,
               'surface': surface}
    return problem


def saddle_model(refines=0, fieldstype=2, options=None):
    """
    构造马鞍曲面上的模型
    """
    from fealpy.geometry import SaddleSurface
    surface = SaddleSurface()
    mesh = surface.init_mesh('meshdata/saddle.mat')
    mesh.uniform_refine(n=refines, surface=surface)
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)
    problem = {
            'objective': obj,
            'x0': mu,
        }
    return problem


def saddle_adaptive_model(refines=0, options=None):
    """
    构造马鞍曲面上的自适应模型
    """
    spheredata = sio.loadmat('init.mat')
    mu = spheredata['mu']

    from fealpy.geometry import SaddleSurface

    surface = SaddleSurface()
    mesh = surface.init_mesh('meshdata/saddle.mat')
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    tritree.uniform_refine(n=refines, options=options, surface=surface)
    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {'objective': obj, 
               'x0': mu,
               'tritree': tritree,
               'surface': surface}
    return problem

def squared_model(refines=0, fieldstype=2, options=None):
    """
    构造方型曲面上的模型
    """
    from fealpy.geometry import SquaredSurface
    surface = SquaredSurface()
    mesh = surface.init_mesh('meshdata/squared.mat')
    mesh.uniform_refine(n=refines, surface=surface)
    obj = SSCFTFEMModel(mesh, surface, options=options)
    mu = obj.init_value(fieldstype=fieldstype)
    problem = {
            'objective': obj,
            'x0': mu,
        }
    return problem

def squared_adaptive_model(refines=0, options=None):
    """
    构造方形曲面上的自适应模型
    """
    spheredata = sio.loadmat('init.mat')
    mu = spheredata['mu']

    from fealpy.geometry import SquaredSurface

    surface = SquaredSurface()
    mesh = surface.init_mesh('meshdata/squared.mat')
    node = mesh.entity('node')
    cell = mesh.entity('cell')
    tritree = Tritree(node, cell)
    tritree.uniform_refine(n=refines, options=options, surface=surface)
    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {'objective': obj, 
               'x0': mu,
               'tritree': tritree,
               'surface': surface}
    return problem

def converge_model(options=None):
    """
    构造收敛的自适应模型
    """
    spheredata = sio.loadmat('25.mat')
    node = spheredata['node']
    cell = spheredata['elem'] - 1
    mu = spheredata['mu']
    rho = spheredata['rho']

    import pickle
    mysurface = open('25surface', 'rb')
    surface = pickle.load(mysurface)
    mysurface.close()

    myfile = open('25tritree', 'rb')
    tritree = pickle.load(myfile)
    myfile.close()

    mesh = tritree.to_conformmesh()
    obj = SSCFTFEMModel(mesh, surface, options=options)
    problem = {
            'objective': obj,
            'x0': mu,
            'tritree': tritree,
            'surface': surface,
            'rho': rho
        }
    return problem

