#!/usr/bin/env python3
#

import sys
import numpy as np
import mpl_toolkits.mplot3d as a3
import pylab as pl
import pickle

from sphere_problem import sphere_model
from SSCFTFEMModel import sscftmodel_options
from fealpy.opt.saddleoptalg import SteepestDescentAlg

moptions = sscftmodel_options(
        nspecies= 2, 
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.2,
        chiAB = 0.25,
        dim = 3,
        p = 1,
        T0 = 40,
        T1 = 160,
        nupdate = 0,
        integrator=6,
        scale = 1.0,
        rdir=sys.argv[1],
        outputdata=False,
        outputfig=True)


problem = sphere_model(radius=3.56, refines=1, fieldstype=5, options = moptions)

options = {
        'MaxIters': 500,
        'MaxFunEvals': 5000,
        'NormGradTol': 1e-4,
        'FunValDiff': 1e-6,
        'StepLength': 2,
        'StepTol': 1e-14,
        'Output': True
        }

optalg = SteepestDescentAlg(problem, options)
optalg.run()

