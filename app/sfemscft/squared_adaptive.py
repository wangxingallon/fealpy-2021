#!/usr/bin/env python3
#

import sys
import numpy as np
import mpl_toolkits.mplot3d as a3
import pylab as pl
import pickle

from gensurface_problem import squared_adaptive_model, converge_model
from SSCFTFEMModel import sscftmodel_options
from fealpy.opt.saddleoptalg import SteepestDescentAlg

moptions = sscftmodel_options(
        nspecies= 2, 
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.2,
        chiAB = 0.25,
        dim = 2,
        p = 2,
        T0 = 40,
        T1 = 160,
        nupdate = 1,
        integrator=6,
        scale = 6.0,
        outputdir=sys.argv[1],
        outputfig=True)

problem = squared_adaptive_model(refines=0, 
        options = moptions)

options = {
        'MaxIters': 500,
        'MaxFunEvals': 5000,
        'NormGradTol': 1e-4,
        'FunValDiff': 1e-6,
        'StepLength': 2,
        'StepTol': 1e-14,
        'Output': True,
        'etarefTol': 0.0
        }


aopts = problem['tritree'].adaptive_options(
        method='mean',
        maxrefine=1,
        maxcoarsen=1,
        theta=1.0,
        p=2,
        maxsize=0.1,
        minsize=1e-6
    )

optalg = SteepestDescentAlg(problem, options)
x, f, g, diff = optalg.run()

model = problem['objective']
tritree = problem['tritree']
surface = problem['surface']

i = 0
while True:
    i += 1
    print('Refine:', i)
    
    eta = model.recover_estimate()
    
    mu0 = model.femspace.function(array=problem['x0'][:, 0])
    mu1 = model.femspace.function(array=problem['x0'][:, 1])
    data0 = tritree.interpolation(mu0)
    data1 = tritree.interpolation(mu1)

    aopts['data'] = {'mu0': data0, 'mu1':data1}

    tritree.adaptive(eta, aopts, surface=surface)
    mesh = tritree.to_conformmesh(aopts)
    print('The number of nodes:', mesh.number_of_nodes())
   
    model.reinit(mesh, surface, options=moptions)
    
    data0 = aopts['data']['mu0']
    data1 = aopts['data']['mu1']
    problem['x0'] = model.femspace.function(dim=2)
    problem['x0'][:, 0] = model.femspace.to_function(data0)
    problem['x0'][:, 1] = model.femspace.to_function(data1)
       
    optalg = SteepestDescentAlg(problem, options)
    x, f, g, diff = optalg.run()
    problem['x0'] = x

    if diff < options['FunValDiff']:
        rho = model.rho
        maxrho = np.max(rho)
        minrho = np.min(rho)
        if (maxrho <= 1.0) & (minrho >= 0.0):
            break

tritree_file=open(str(int(moptions['chiN']))+'tritree', 'wb')
pickle.dump(problem['tritree'], tritree_file)
tritree_file.close()

surface_file=open(str(int(moptions['chiN']))+'surface', 'wb')
pickle.dump(problem['surface'], surface_file)
surface_file.close()

model.save_data(fname=str(int(moptions['chiN']))+'.mat', filename=str(int(moptions['chiN']))+'.txt')

fig = pl.figure()
axes = a3.Axes3D(fig)
mesh.add_plot(axes, linewidths=0.5)
pl.show()
