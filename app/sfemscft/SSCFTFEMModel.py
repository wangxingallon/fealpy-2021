import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt

import vtk
import vtk.util.numpy_support as vnp

from fealpy.functionspace import ParametricLagrangeFiniteElementSpace
from fealpy.quadrature.FEMeshIntegralAlg import FEMeshIntegralAlg
from fealpy.recovery import FEMFunctionRecoveryAlg
from fealpy.timeintegratoralg.timeline import ChebyshevTimeLine

from queue import Queue
from fealpy.solver import MatlabSolver
from timeit import default_timer as timer

from ParabolicSFEMSolver import ParabolicSFEMSolver2d

def sscftmodel_options(
        nspecies=2,
        nblend = 1,
        nblock = 2,
        ndeg = 100,
        fA = 0.20,
        chiAB = 0.25,
        dim = 2,
        p = 1,
        T0 = 4,
        T1 = 16,
        nupdate = 1,
        integrator=3,
        scale=1.0,
        rdir=None,
        outputdata=False,
        outputfig=False):
    """
    Get the options used in model.
    """
    options = {
            'nspecies': nspecies,
            'nblend': nblend,
            'nblock': nblock,
            'ndeg': ndeg,
            'fA': fA,
            'chiAB': chiAB,
            'dim': dim,
            'p': p,
            'T0': T0,
            'T1': T1,
            'nupdate': nupdate,
            'integrator': integrator,
            'scale': scale,
            'rdir': rdir,
            'outputdata': outputdata,
            'outputfig': outputfig
            }

    options['chiN'] = options['chiAB']*options['ndeg']
    return options

class SSCFTFEMModel():
    def __init__(self, mesh, surface, options=None):
        if options == None:
            self.options = sscftmodel_options()
        else:
            self.options = options

        self.femspace =  ParametricLagrangeFiniteElementSpace(mesh, p=options['p'])
        self.mesh = self.femspace.mesh
        self.area = self.mesh.entity_measure()
        self.totalArea = np.sum(self.area)
        self.integrator = self.mesh.integrator(options['integrator'])
        self.nupdate = options['nupdate']

        fA = self.options['fA']
        T0 = options['T0']
        T1 = options['T1']
        self.timeline0 = ChebyshevTimeLine(0, fA, T0)
        self.timeline1 = ChebyshevTimeLine(fA, 1, T1)
        N = T0 + T1 + 1
        gdof = self.femspace.number_of_global_dofs()
        print('NN',gdof)

        self.q0 = np.zeros((gdof, N), dtype=self.mesh.ftype)
        self.q1 = np.zeros((gdof, N), dtype=self.mesh.ftype)

        self.rho = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.grad = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.w = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.sQ = 0.0
        self.eta_ref = 0.0
        self.count = 0
        self.A = self.femspace.stiff_matrix()
        self.M = self.femspace.mass_matrix()
        self.smodel = ParabolicSFEMSolver2d(self.A, self.M)

    def recover_estimate(self):
        qA = self.femspace.function(array=self.q0[:, -1])
        rguh = self.femspace.grad_recovery(qA)
        def f(x):
            return np.sum(rguh.value(x)**2, axis=-1)
        etaA = self.femspace.integralalg.integral(f, celltype=True)

        qB = self.femspace.function(array=self.q1[:, -1])
        rguh = self.femspace.grad_recovery(qB)
        etaB = self.femspace.integralalg.integral(f, celltype=True)
        eta = np.sqrt(etaA) + np.sqrt(etaB)
        return eta
   
    def compute_eta_ref(self):
        eta = self.estimate()
        self.eta_ref = np.std(eta)/(np.max(eta) - np.min(eta))
        print('第'+str(self.count)+'次迭代的etaref:', self.eta_ref)

    def estimate(self):
        qA = self.femspace.function(array=self.q0[:, -1])
        qB = self.femspace.function(array=self.q1[:, -1])

        bc = np.array([1/3]*3, dtype=self.mesh.ftype)
        gradA = qA.grad_value(bc)
        etaA = np.sum(gradA**2, axis=-1)*self.area
        gradB = qB.grad_value(bc)
        etaB = np.sum(gradB**2, axis=-1)*self.area
        eta = np.sqrt(etaA) + np.sqrt(etaB)
        return eta

    def estimate_rho(self):
        integralalg = FEMeshIntegralAlg(self.integrator, self.mesh)
        qf = self.integrator
        bcs, ws = qf.quadpts, qf.weights
        rhoA = self.femspace.function(array=self.rho[:, 0])
        rhoB = self.femspace.function(array=self.rho[:, 1])
        gradA = rhoA.grad_value(bcs) 
        gradB = rhoB.grad_value(bcs)
        etaA = integralalg.L2_norm_1(gradA, celltype=True) 
        etaB = integralalg.L2_norm_1(gradB, celltype=True)
        eta = etaA + etaB
        return eta
    
    def estimate_q(self):
        integralalg = FEMeshIntegralAlg(self.mesh, self.integrator)
        qf = self.integrator
        bcs, ws = qf.quadpts, qf.weights
        qA = self.femspace.function(array=self.q0[:, -1])
        qB = self.femspace.function(array=self.q1[:, -1])
        gradA = qA.grad_value(bcs) 
        gradB = qB.grad_value(bcs)
        etaA = integralalg.L2_norm_1(gradA, celltype=True) 
        etaB = integralalg.L2_norm_1(gradB, celltype=True)
        eta = etaA + etaB
        return eta
    
    def estimate_w(self):
        integralalg = FEMeshIntegralAlg(self.integrator, self.mesh)
        qf = self.integrator
        bcs, ws = qf.quadpts, qf.weights
        wA = self.femspace.function(array=self.w[:, 0])
        wB = self.femspace.function(array=self.w[:, 1])
        gradA = wA.grad_value(bcs) 
        gradB = wB.grad_value(bcs)
        etaA = integralalg.L2_norm_1(gradA, celltype=True) 
        etaB = integralalg.L2_norm_1(gradB, celltype=True)
        eta = etaA + etaB
        return eta

    def reinit(self, mesh, surface, options=None):
        if options is not None:
            self.options = options

        self.femspace =  ParametricLagrangeFiniteElementSpace(mesh, p=options['p'])
        self.mesh = self.femspace.mesh
        self.area = self.mesh.cell_area() 
        self.totalArea = np.sum(self.area) 
        self.integrator = self.mesh.integrator(self.options['integrator'])
        self.nupdate = options['nupdate']

        fA = self.options['fA']
        T0 = options['T0']
        T1 = options['T1']
        self.timeline0 = ChebyshevTimeLine(0, fA, T0)
        self.timeline1 = ChebyshevTimeLine(fA, 1, T1)
        N = T0 + T1 + 1
        gdof = self.femspace.number_of_global_dofs()

        self.q0 = np.zeros((gdof, N), dtype=self.mesh.ftype)
        self.q1 = np.zeros((gdof, N), dtype=self.mesh.ftype)

        self.rho = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.grad = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.w = np.zeros((gdof, 2), dtype=self.mesh.ftype)
        self.sQ = 0.0
        self.eta_ref = 0.0

        self.A = self.femspace.stiff_matrix()
        self.M = self.femspace.mass_matrix()
        self.smodel = ParabolicSFEMSolver2d(self.A, self.M)

    def init_value(self, fieldstype=2):
        gdof = self.femspace.number_of_global_dofs()
        mesh = self.femspace.mesh

        chiN = self.options['chiN']
        fields = np.zeros((gdof, 2), dtype=mesh.ftype)
        mu = np.zeros((gdof, 2), dtype=mesh.ftype)
        w = np.zeros((gdof, 2), dtype=mesh.ftype)
        if fieldstype == 1:
            fields[:12, 1] = 1
        elif fieldstype == 2:
            fields[:, 0] = chiN*(-1 + 2*np.random.rand(gdof))
            fields[:, 1] = chiN*(-1 + 2*np.random.rand(gdof))
        elif fieldstype == 3:
            ipoint = self.femspace.interpolation_points()
            k = 5
            pi = np.pi
            theta = np.arctan2(ipoint[:,1], ipoint[:,0])
            theta = (theta >= 0)*theta + (theta <0)*(theta + 2*pi)
            fields[:, 1] = chiN*np.sin(k*theta)
        elif fieldstype == 4:
            k  = 8
            pi = np.pi
            ipoint = self.femspace.interpolation_points()

            theta = np.arctan2(ipoint[:,1], ipoint[:,0])
            theta = (theta >= 0)*theta + (theta <0)*(theta + 2*pi)

            phi = np.arctan(ipoint[:, 2], ipoint[:, 1])
            phi = (phi >= 0)*phi + (phi <0)*(phi + 2*pi)

            fields[:, 1] = chiN*np.sin(k*theta+k*phi)
        elif fieldstype == 5:
            pi = np.pi
            ipoint = self.femspace.interpolation_points()
            fields[:, 1] = chiN*(np.sin(0.5*pi*ipoint[:,0]))
        elif fieldstype == 6:
            k = 5
            ipoint = self.femspace.interpolation_points()
            fields[:, 1] = chiN*(np.sin(k*ipoint[:, 0]))
        elif fieldstype == 7:
            ipoint = self.femspace.interpolation_points()
            k = 8
            pi = np.pi
            theta = np.arctan2(ipoint[:,1], ipoint[:,0])
            theta = (theta >= 0)*theta + (theta <0)*(theta + 2*pi)
            fields[:, 1] = chiN*np.sin(k*theta)
        elif fieldstype == 8:
            k = 5
            ipoint = self.femspace.interpolation_points()
            fields[:, 1] = chiN*(np.sin(k*ipoint[:, 2]))
        elif fieldstype == 9:
            k = 5
            ipoint = self.femspace.interpolation_points()
            fields[:, 1] = chiN*(np.sin(k*ipoint[:, 1]))

        
        w[:, 0] = fields[:, 0] - fields[:, 1]
        w[:, 1] = fields[:, 0] + fields[:, 1]

        mu[:, 0] = 0.5*(w[:, 0] + w[:, 1])
        mu[:, 1] = 0.5*(w[:, 1] - w[:, 0])
        return mu

    def __call__(self, mu, returngrad=True):
        """
        目标函数，给定外场，计算哈密尔顿量及其梯度
        """
        self.w[:, 0] = mu[:, 0] - mu[:, 1]
        self.w[:, 1] = mu[:, 0] + mu[:, 1]

        start = timer()
        self.compute_propagator()
        print('Times for PDE solving:', timer() - start)
        #self.compute_eta_ref()
        self.compute_singleQ()
        self.compute_density()

        u = self.femspace.function(array=mu[:, 0]).value
        mu1_int = self.integral_space(u)

        u0 = self.femspace.function(array=mu[:, 1]).value
        u = lambda x : u0(x)**2
        mu2_int = self.integral_space(u)


        chiN = self.options['chiN']
        self.H = -mu1_int + mu2_int/chiN
        self.H = self.H/self.totalArea - np.log(self.sQ)
        #self.save_data(fname=self.options['rdir']+'/'+str(int(chiN))+'spheredata'+str(self.count)+'.mat', filename='log.txt')
        self.mesh.nodedata['rho'] = self.rho[:, 0]
        #self.mesh.to_vtk(fname=self.options['rdir']+'/'+str(int(chiN)) + str(self.count)+'.vtu')
        self.grad[:, 0] = self.rho[:, 0]  + self.rho[:, 1] - 1.0
        self.grad[:, 1] = 2.0*mu[:, 1]/chiN - self.rho[:, 0] + self.rho[:, 1]
        self.count += 1
        if returngrad:
            return self.H, self.grad
        else:
            return self.H

    def compute_propagator(self):
        n0 = self.timeline0.NL
        n1 = self.timeline1.NL

        w = self.femspace.function(array=self.w[:, 0]).value
        F0 = self.femspace.mass_matrix(c=w)
        smodel0 = ParabolicSFEMSolver2d(self.A,self.M,F0)

        w = self.femspace.function(array=self.w[:, 1]).value
        F1 = self.femspace.mass_matrix(c=w)
        smodel1 = ParabolicSFEMSolver2d(self.A,self.M,F1)
        self.q0[:, 0] = 1.0
        self.timeline0.time_integration(self.q0[:, 0:n0], smodel0, self.nupdate)
        self.timeline1.time_integration(self.q0[:, n0-1:], smodel1, self.nupdate)
        self.q1[:, 0] = 1.0
        self.timeline1.time_integration(self.q1[:, 0:n1], smodel1, self.nupdate)
        self.timeline0.time_integration(self.q1[:, n1-1:], smodel0, self.nupdate)
 
    def compute_singleQ(self):
        u = self.femspace.function(array=self.q0[:, -1]).value
        self.sQ = self.integral_space(u)/self.totalArea
        print('Q', self.sQ)

    def compute_density(self):
        q = self.q0*self.q1[:, -1::-1]
        n0 = self.timeline0.NL
        self.rho[:, 0] = self.timeline0.dct_time_integral(q[:, 0:n0],return_all=False)/self.sQ
        self.rho[:, 1] = self.timeline0.dct_time_integral(q[:, n0-1:],return_all=False)/self.sQ

    def integral_space(self, u):
        mesh = self.mesh
        qf = self.integrator
        bcs, ws = qf.quadpts, qf.weights
        val = u(bcs)
        Q = np.einsum('i, ij, j', ws, val, self.area)
        return Q

    def integral_space1(self, u):
        mesh = self.mesh
        qf = self.integrator
        bcs, ws = qf.quadpts, qf.weights
        rm = mesh.reference_cell_measure()
        G = mesh.first_fundamental_form(bcs)
        d = np.sqrt(np.linalg.det(G))
        ps = mesh.bc_to_point(bcs, etype='cell')
        val = u(ps)
        Q = np.einsum('q, qc, qc->c', ws*rm, val, d)
        return Q


    def output(self, tag, queue=None, stop=False):
        if queue is not None:
            if not stop:
                queue.put({tag:self.rho[:, 0]})
            else:
                queue.put(-1)
        elif self.options['rdir'] is not None:
            if (self.options['outputfig'] is True) and (tag is not ''):
                mesh = self.mesh
                cell2node = mesh.ds.cell_to_node()
                NN = cell2node.shape[1]
                d = np.asarray(cell2node@self.rho[:NN, 0])/3
                fig = plt.figure()
                axes = fig.gca(projection='3d')
                mesh.add_plot(axes, cellcolor=d, linewidths=0, showcolorbar=True)
                plt.savefig(self.options['rdir'] + '/'+str('chiN') + str(int(self.options['chiN'])) + tag +'.png')
                plt.close()


    def save_data(self, fname='spheredata.mat', filename='log.txt'):  
        mu = self.w.copy()
        mu[:, 0] = 0.5*(self.w[:, 0] + self.w[:, 1]) 
        mu[:, 1] = 0.5*(self.w[:, 1] - self.w[:, 0]) 
        node = self.mesh.node
        cell = self.mesh.ds.cell
        rho = self.rho
        Q    = self.sQ
        H    = self.H
        data = {'mu':    mu,
                'node':  node,
                'elem':  cell+1,
                'rho' :  rho,
                'Q'   :  Q,
                'H'   :  H
                }
        f = open(filename, 'w') 
        string = 'Q:%.15e H:%.15e \n' %(Q, H)
        f.write(string)
        sio.savemat(fname, data)
