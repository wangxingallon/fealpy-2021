import numpy as np
from scipy.sparse import bmat
from scipy.sparse.linalg import spsolve

import vtk
import vtk.util.numpy_support as vnp

from fealpy.decorator import barycentric
from fealpy.functionspace import ParametricLagrangeFiniteElementSpace

class PhaseFieldCrystalModel():
    def __init__(self, mesh, options):

        self.options = options
        self.mesh = mesh 
        self.space = ParametricLagrangeFiniteElementSpace(mesh, options['order'], spacetype='C')
        self.gdof = self.space.number_of_global_dofs()

        self.A = self.space.stiff_matrix()
        self.M = self.space.mass_matrix()
        

        self.uh0 = self.space.function()
        self.uh1 = self.space.function()
        self.utmp = self.space.function()

        self.ftype = mesh.ftype
        self.itype = mesh.itype
        self.dt = 2

        #self.uh = np.ones((self.gdof, N), dtype=self.ftype)
        
        self.H = []
        self.G = []

    def options(
            self,
            c=1,
            s=0.3,
            epsilon=-1,
            order=1
            ):

        options = {
                'c': c,
                's': s,
                'epsilon': epsilon,
                'order': order
            }
        return options

    def set_init_solution(self, u):
        self.uh0[:] = u
    
    def current_time_step_length(self, w, u0, u1):
        s = self.options['s']
        epsilon = self.options['epsilon']
        utmp = self.space.function(dim=1,array=self.utmp)
        uhh1 = self.uh1
        
        @barycentric
        def f0(bcs):
            val0 = utmp(bcs)
            val1 = uhh1(bcs)
            val = (val0 + val1)**2/2
            val += epsilon*val0**2/2
            val -= s*val0**3/6
            val += val0**4/24
            return val

        H = self.space.integralalg.mesh_integral(f0)

        gradf0 = epsilon*u0 - s*u0**2/2 + u0**3/6
        gradf1 = epsilon*u1 - s*u1**2/2 + u1**3/6
#
#       # @barycentric
#       # def f1(bcs):
#       #     val = u0(bcs)
#       #     return s*val**2/2 - val**3/6
#       # 
#       # gradf0 = epsilon*self.M*u0 
#       # gradf0 -= self.space.source_vector(f1) 
#
#       # @barycentric
#       # def f2(bcs):
#       #     val = u1(bcs)
#       #     return s*val**2/2 - val**3/6
#       # 
#       # gradf1 = epsilon*self.M*u1
#       # gradf1 -= self.space.source_vector(f2) 
#

 

        s = u1 - u0
        print('s',s)
        v = gradf1 - gradf0
        print('v',v)
        self.dt = np.sum(s*s)/np.sum(s*v)
        #self.dt = np.sum(v*s)/np.sum(v*v)
        dtmin = 1
        dtmax = 4
        if (self.dt < dtmin):
            self.dt = dtmin

        if (self.dt > dtmax):
            self.dt = dtmax

        k=0
        while 1:
            k+=1
            print('k',k)
            self.one_step_solve_APG(w, u0, u1)
            uh2 = self.uh0.copy()
            uh2 = self.space.function(dim=1,array=uh2)
            self.Hamilton()
            EDiff = H - self.H[-1]

            print("ediff", EDiff)
            
            @barycentric
            def f1(bcs):
                val0 = utmp(bcs)
                val1 = uh2(bcs)
                val = (val0 - val1)**2
                return val
            uDiff = self.space.integralalg.mesh_integral(f1)
            
            if (EDiff < 1e-10*uDiff):
                print("Linear")
                print('dt',self.dt)
                self.dt  = 0.618*self.dt
            else:
                print('dt',self.dt)
                #self.dt =3
                break
            #print("utmp",self.utmp)


    def get_current_left_matrix(self):
        dt = self.dt
        A = self.A
        M = self.M
        S = bmat([[M + dt*(M - 2*A), -dt*A], [A, M]], format='csr')
        return S

    def get_current_left_matrixCN(self):
        dt = self.dt
        A = self.A
        M = self.M
        S = bmat([[M + dt*(1/2*M - A), -dt*A], [A, M]], format='csr')
        return S


    def get_current_right_vector(self):
        dt = self.dt
        gdof = self.space.number_of_global_dofs()

        s = self.options['s']
        epsilon = self.options['epsilon']

        uh0 = self.uh0 
        M = self.M
        F = np.zeros((2*gdof, ), dtype=self.ftype)
        F[:gdof] = M@uh0
#        F[:gdof] *= 1 - dt*epsilon

        @barycentric
        def f(bcs):
            val = uh0(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += dt*self.space.source_vector(f)
        return F

    def get_current_right_vector_APG(self, w, uh0, uh1):
        dt = self.dt
        gdof = self.space.number_of_global_dofs()
        s = self.options['s']
        epsilon = self.options['epsilon']

        self.utmp = (1-w)*uh1+w*uh0
        #print("utmp",self.utmp)
        utmp = self.space.function(dim=1,array=self.utmp)
        M = self.M
        F = np.zeros((2*gdof, ), dtype=self.ftype)
        F[:gdof] = M@utmp
#        F[:gdof] *= 1 - dt*epsilon

        @barycentric
        def f(bcs):
            val = utmp(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += dt*self.space.source_vector(f)
        return F

    def get_current_right_vectorCN(self):
        dt = self.current_time_step_length()
        gdof = self.space.number_of_global_dofs()

        s = self.options['s']
        epsilon = self.options['epsilon']

        uh0 = self.uh0 
        M = self.M
        A = self.A
        F = np.zeros((2*gdof, ), dtype=self.ftype)
        F[:gdof] = (M-1/2*dt*M+dt*A)@uh0
#        F[:gdof] *= 1 - dt*epsilon

        @barycentric
        def f(bcs):
            val = uh0(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += dt*self.space.source_vector(f)
        return F


    def one_step_solve_semiimplict(self):
        """

        Notes
        -----
            求解一个时间层的数值解
        """

        gdof = self.space.number_of_global_dofs()
        A = self.get_current_left_matrix()
        F = self.get_current_right_vector()
        x = spsolve(A, F)
        self.uh0[:] = x[:gdof]
        self.uh1[:] = x[gdof:]
        
    def one_step_solve_APG(self, w, uh0, uh1):
        """

        Notes
        -----
            求解一个时间层的数值解
        """
        gdof = self.space.number_of_global_dofs()
        A = self.get_current_left_matrix()
        F = self.get_current_right_vector_APG(w, uh0, uh1)
        x = spsolve(A, F)
        self.uh0[:] = x[:gdof]
        self.uh1[:] = x[gdof:]

    def post_process(self):
        area = np.sum(self.space.cellmeasure)
        self.uh0 -= self.space.integralalg.mesh_integral(self.uh0)/area

    def Hamilton(self):
        s = self.options['s']
        epsilon = self.options['epsilon']
        uh0 = self.uh0
        uh1 = self.uh1

        @barycentric
        def f0(bcs):
            val0 = uh0(bcs)
            val1 = uh1(bcs)
            val = (val0 + val1)**2/2
            val += epsilon*val0**2/2
            val -= s*val0**3/6
            val += val0**4/24
            return val

        H = self.space.integralalg.mesh_integral(f0)
        self.H.append(H)
        
        @barycentric
        def f1(bcs):
            val = uh0(bcs)
            return -s*val**2/2 + val**3/6
        grad = self.M*uh0 
        grad += epsilon*self.M*uh0
        grad -= 2*self.A*uh0 
        grad += self.space.source_vector(f1) 
        grad -= self.A*uh1

        grad = self.space.function(dim=1, array=grad)

        @barycentric
        def f2(bcs):
            val = grad(bcs)
            return val**2
        grad = self.space.integralalg.mesh_integral(f2)
        self.G.append(grad)

    def solve_APG(self, disp=True, output=False, rdir='.', step=1, postprocess=False):
        """

        Notes
        -----

        计算所有的时间层。
        """

        dt = self.dt
        uh0 = self.uh0.copy()
        self.one_step_solve_semiimplict()
        current = 0
        theta0 = 1
 
        if postprocess:
            self.post_process()

        uh1 = self.uh0.copy()
        self.Hamilton()

        if output:
            fname = rdir + '/step_'+ str(current).zfill(10) + '.vtu'
            print(fname)
            self.write_to_vtk(fname)

        if disp:
            print(current, "Current Hamilton energy ", self.H[-1], " with gradient ",
                    self.G[-1] )
            print("Max phase value:", np.max(self.uh0))
            print("Min phase value:", np.min(self.uh0))

        while 1:
            current += 1
            print("timeline\n", current)
            theta1 = np.sqrt(theta0**2+0.25*theta0**4)-0.5*theta0**2
            w = theta0*(1-theta0)/(theta0**2+theta1)
            print("uh0", uh0)
            print("uh1", uh1)
            self.current_time_step_length(w, uh0, uh1)
            v = self.uh0.copy()
            
            if postprocess:
                self.post_process()

            print("dt\n", self.dt)
            if disp:
                print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
                print("Max phase value:", np.max(self.uh0))
                print("Min phase value:", np.min(self.uh0))

            if output & (current%step == 0):
                fname = rdir + '/step_'+ str(current).zfill(10) + '.vtu'
                print(fname)
                self.write_to_vtk(fname)

            uh1 = self.space.function(dim=1,array=uh1)
            v = self.space.function(dim=1,array=v)
            @barycentric
            def f2(bcs):
                val0 = v(bcs)
                val1 = uh1(bcs)
                val = (val0 - val1)**2
                return val
            
            uDiff = self.space.integralalg.mesh_integral(f2)
            

            print("Ediff", self.H[-1]-self.H[-2])
            if (self.H[-1]-self.H[-2] > -1e-12*uDiff):
                print("restart", self.H[-1]-self.H[-2])
                theta0 = 1
                del self.H[-1]
            else:
                theta0 = theta1
                uh0 = uh1
                uh1 = v
            
            if (abs(self.H[-1]-self.H[-2]) < 1e-12):
                print("end")
                break



    def solve(self, disp=True, output=False, rdir='.', step=1, postprocess=False):
        """

        Notes
        -----

        计算所有的时间层。
        """

        timeline = self.timeline
        dt = timeline.current_time_step_length()
        timeline.reset() # 时间置零
        self.one_step_solve()

        if postprocess:
            self.post_process()

        self.Hamilton()

        if output:
            fname = rdir + '/step_'+ str(timeline.current).zfill(10) + '.vtu'
            print(fname)
            self.write_to_vtk(fname)

        if disp:
            print(timeline.current, "Current Hamilton energy ", self.H[-1], " with gradient ",
                    self.G[-1] )
            print("Max phase value:", np.max(self.uh0))
            print("Min phase value:", np.min(self.uh0))

        while not timeline.stop():
            self.one_step_solve()
            if postprocess:
                self.post_process()
            self.Hamilton()
            timeline.current += 1

            if disp:
                print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
                print("Max phase value:", np.max(self.uh0))
                print("Min phase value:", np.min(self.uh0))

            if output & (timeline.current%step == 0):
                fname = rdir + '/step_'+ str(timeline.current).zfill(10) + '.vtu'
                print(fname)
                self.write_to_vtk(fname)
        timeline.reset()

    def write_to_vtk(self, fname):
        self.mesh.nodedata['uh0'] = self.uh0
        self.mesh.nodedata['uh1'] = self.uh1
        self.mesh.to_vtk(fname=fname)

