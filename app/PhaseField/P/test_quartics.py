import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as a3
import sys
from matplotlib import cm

from fealpy.geometry import QuarticsSurface, ScaledSurface
from fealpy.timeintegratoralg import UniformTimeLine
from fealpy.mesh.LagrangeTriangleMesh import LagrangeTriangleMesh
from fealpy.mesh.TriangleMesh import TriangleMesh
from PhaseFieldCrystalModel import PhaseFieldCrystalModel
#from fealpy.fem import PhaseFieldCrystalModel

n = int(sys.argv[1])#网格加密次数
p = int(sys.argv[2])#基函数次数
f = int(sys.argv[3])#初值选取
R = float(sys.argv[4])#半径大小


options1 = {'c': 1, 's': 0.3, 'epsilon': -0.13, 'order':p}
options2 = {'c': 1, 's': 0, 'epsilon': -1, 'order':p}

if f==1 or f==2:
    options = options1
else:
    options = options2

surface = QuarticsSurface()
mesh = surface.init_mesh('meshdata/quartics.mat')
mesh.uniform_refine(n=n, surface=surface)
node = mesh.entity('node')
cell = mesh.entity('cell')

scaledsurface = ScaledSurface(surface, scale=R)
lmesh = LagrangeTriangleMesh(node*R, cell, surface=scaledsurface, p=options['order'])

model = PhaseFieldCrystalModel(lmesh, options)

print("gdof", model.gdof)
#初值选取
def u(fieldstype=1):
    u = model.space.function()

    if fieldstype == 1:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5* np.cos(np.pi*p[..., 0])*np.cos(np.pi*p[..., 1])*np.cos(np.pi*p[..., 2]))
    elif fieldstype == 2:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5*np.cos(p[..., 0])*np.cos(p[...,1])*np.cos(p[..., 2]))

    elif fieldstype == 3:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5*np.cos(np.pi*p[..., 0]))
    elif fieldstype == 4:
        u[:] = model.space.interpolation(lambda p: np.cos(5*p[..., 0]))
    return u


data = model.set_init_solution(u(fieldstype=f))
#model.solve(output=True, postprocess=True)

model.solve_APG(output=True, postprocess=True, k=2)

#x = np.linspace(0,N+1,N+1)
#
#fig3 = plt.figure(3)
#axes3 = fig3.add_subplot(2,1,1)
#plt.plot(x,model.H, 'g--')
#plt.title('Hamilton')
#axes4 = fig3.add_subplot(2,1,2)
#plt.plot(x,model.G,'r--')
#plt.title('grad')
#
#plt.show()

