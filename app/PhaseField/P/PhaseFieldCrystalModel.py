import numpy as np
from scipy.sparse import bmat
from scipy.sparse.linalg import spsolve
from scipy.sparse.linalg import inv

import vtk
import vtk.util.numpy_support as vnp

from fealpy.decorator import barycentric
from fealpy.functionspace import ParametricLagrangeFiniteElementSpace

class PhaseFieldCrystalModel():
    def __init__(self, mesh, options):

        self.options = options
        self.mesh = mesh 
        self.space = ParametricLagrangeFiniteElementSpace(mesh, options['order'], spacetype='C')
        self.gdof = self.space.number_of_global_dofs()

        self.A = self.space.stiff_matrix()
        self.M = self.space.mass_matrix()
        self.B = self.space.stiff_matrix()

        self.uh0 = self.space.function()
        self.uh1 = self.space.function()
        self.utmp = self.space.function()

        self.ftype = mesh.ftype
        self.itype = mesh.itype
        
        self.dt = 2

        #self.uh = np.ones((self.gdof, N), dtype=self.ftype)
        
        self.H = []
        self.G = []

    def options(
            self,
            c=1,
            s=0.3,
            epsilon=-1,
            order=1
            ):

        options = {
                'c': c,
                's': s,
                'epsilon': epsilon,
                'order': order
            }
        return options

    def set_init_solution(self, u):
        self.uh0[:] = u
    
    def one_step_APG(self, w, u0, u1, k):
        s = self.options['s']
        epsilon = self.options['epsilon']
        c = self.options['c']
        self.dt = 4
        while 1:
            if (w==0):
                self.one_step_solve_APG(w, u0, u1, 2)
                self.post_process()
                self.Hamilton()
            else:
                u0= self.space.function(dim=1, array=u0)
                u1= self.space.function(dim=1, array=u1)
                
            #@barycentric
            #def f(bcs):
            #    val = epsilon*u1(bcs)- 0.5*s*u1(bcs)**2 + u1(bcs)**3/6
            #    val -= epsilon*u0(bcs)- 0.5*s*u0(bcs)**2 + u0(bcs)**3/6
            #    return val**2
           
            #@barycentric
            #def f1(bcs):
            #    val1 = epsilon*u1(bcs)- 0.5*s*u1(bcs)**2 + u1(bcs)**3/6
            #    val1 -= epsilon*u0(bcs)- 0.5*s*u0(bcs)**2 + u0(bcs)**3/6
            #    val = (u1(bcs)-u0(bcs))*val1
            #    return val
            #self.dt = self.space.integralalg.mesh_integral(f1)
            #self.dt /= self.space.integralalg.mesh_integral(f)
            #@barycentric
            #def f(bcs):
            #    val= epsilon*u1(bcs)- 0.5*s*u1(bcs)**2 + u1(bcs)**3/6
            #    return val**2
            #@barycentric
            #def f1(bcs):
            #    val= epsilon*u1(bcs)- 0.5*s*u1(bcs)**2 + u1(bcs)**3/6
            #    val1 =  epsilon*u0(bcs)- 0.5*s*u0(bcs)**2 + u0(bcs)**3/6
            #    val = val*(val-val1)
            #    return val
            #self.dt = -self.dt*self.space.integralalg.mesh_integral(f)
            #self.dt /= self.space.integralalg.mesh_integral(f1)
        #        print('t1',self.dt)
        #        dtmin = 2
        #        dtmax = 4
        #        if (self.dt < dtmin):
        #            self.dt = dtmin

        #        if (self.dt > dtmax):
        #            self.dt = dtmax
            
            ##线搜索
                if (k==4):
                    a = self.one_step_norm2(w, u0, u1)
                    print('a', a)
                else:
                    a=None
                
                self.one_step_solve_APG(w, u0, u1, k, a)
                self.post_process()
                
                self.Hamilton()
            
            utmp = (1-w)*u1+w*u0
                
            uh1 = spsolve(self.M, -self.A@utmp)
            utmp = self.space.function(dim=1, array=utmp)
            uh1 = self.space.function(dim=1, array=uh1)
            
            @barycentric
            def f0(bcs):
                val0 = utmp(bcs)
                val1 = uh1(bcs)
                val = c*(val0 + val1)**2/2
                val += epsilon*val0**2/2
                val -= s*val0**3/6
                val += val0**4/24
                return val

            H = self.space.integralalg.mesh_integral(f0)/np.sum(self.space.cellmeasure)

            EDiff = H - self.H[-1]
            print("H", H, self.H[-1])
            print("ediff > 0:", EDiff)
            
            uh2 = self.uh0.copy()
            uh2 = self.space.function(dim=1,array=uh2)
            
            @barycentric
            def f1(bcs):
                val0 = utmp(bcs)
                val1 = uh2(bcs)
                val = (val0 - val1)**2
                return val
            uDiff = self.space.integralalg.mesh_integral(f1)
            
            if (EDiff < 1e-10*uDiff):
                print("Linear")
                print('t',self.dt)
                self.dt  = 0.618*self.dt
                del self.H[-1]
                del self.G[-1]
            else:
                break
    
    def one_step_norm2(self, w, uh0, uh1):
        M = self.M
        A = self.A
        B = self.B
        """
        上一步的uh作为a初值
        """
        uh = self.space.function(dim=1, array=uh1)
        
        @barycentric
        def f(bcs):
            val = uh(bcs)
            return val**2
        a = self.space.integralalg.mesh_integral(f)
        print('a0', a)
        """
        得到右端D
        """
        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']

        utmp = (1-w)*uh1+w*uh0
        utmp = self.space.function(dim=1, array=utmp)
         
        @barycentric
        def f1(bcs):
            val = utmp(bcs)
            return val**2

        F = self.space.integralalg.mesh_integral(f1)

        D = (F+1)*M@utmp
        
        @barycentric
        def f0(bcs):
            val = utmp(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        
        D += self.dt*self.space.source_vector(f0)
        """
        牛顿法
        """

        k=0
        while 1:
            calA =(a+1)*M + self.dt*(M - 2*A + B)

            invA = inv(calA)
            invAD = self.space.function(dim=1, array=invA@D)
            
            @barycentric
            def f2(bcs):
                val = invAD(bcs)
                return val**2
            g = self.space.integralalg.mesh_integral(f2) - a

            
            dtmp0 = invD*(M*invA+invA*M)

            dg = np.sum(dtmp0*invD)
            
            dg += -1

            a = a-g/dg
            k += 1
            print('----------------tol----------', k, abs(-g/dg))
            if (abs(-g/dg) < 1e-5):
                break
        return a


    def get_current_left_matrix(self):
        dt = self.dt
        c = self.options['c']
        A = self.A
        M = self.M
        S = bmat([[M + c*dt*(M - 2*A), -c*dt*A], [A, M]], format='csr')
        return S
    
    def get_current_left_matrix_APG(self, k, a=None):
        dt = self.dt
        c = self.options['c']
        A = self.A
        M = self.M
        if (k == 2):
            S = bmat([[M + c*dt*(M - 2*A), -c*dt*A], [A, M]], format='csr')
        if (k == 4):
            S = bmat([[(a+1)*M + c*dt*(M - 2*A), -c*dt*A], [A, M]], format='csr')
        return S
    
    def get_current_left_matrix_PCG(self, mu):
        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']
        A = self.A
        M = self.M

        uh = -s*self.uh0+0.5*self.uh0**2
        uh0 = self.space.function(array=uh).value
        F = self.space.mass_matrix(c=uh0)

        S = bmat([[(c + epsilon)*M-2*c*A+F+mu*np.eye(self.gdof)*M, -c*A], [A, M]], format='csr')
        return S
    
    def get_Precondition(self, mu):
        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']
        A = self.A
        M = self.M
       
        con = epsilon - s*self.uh0 + 0.5*self.uh0**2
        con = self.space.function(array=con).value
        
        F = self.space.mass_matrix(c=con)

        S = bmat([[c*M -2*c*A +mu*np.eye(self.gdof)*M + F.max()*np.eye(self.gdof), -A], [A, M]], format='csr')
        return S

    def get_current_right_vector_PCG(self):
        gdof = self.space.number_of_global_dofs()

        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']

        uh0 = self.space.function(dim=1, array=self.uh0)
        M = self.M
        F = np.zeros((2*gdof, ), dtype=self.ftype)
        uh1 = spsolve(M, -self.A@uh0)
        F[:gdof] = -c*M@uh0 + 2*c*self.A@uh0 + c*self.A@uh1

        @barycentric
        def f(bcs):
            val = uh0(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += self.space.source_vector(f)
        return F

    def get_current_right_vector(self):
        dt = self.dt
        gdof = self.space.number_of_global_dofs()

        s = self.options['s']
        epsilon = self.options['epsilon']

        uh0 = self.uh0 
        M = self.M
        F = np.zeros((2*gdof, ), dtype=self.ftype)
        F[:gdof] = M@uh0

        @barycentric
        def f(bcs):
            val = uh0(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += dt*self.project(self.space.source_vector(f))
        return F

    def get_current_right_vector_APG(self, w, uh0, uh1, k):
        dt = self.dt
        gdof = self.space.number_of_global_dofs()
        s = self.options['s']
        epsilon = self.options['epsilon']
        M = self.M
        F = np.zeros((2*gdof, ), dtype=self.ftype)

        self.utmp = (1-w)*uh1+w*uh0
        utmp = self.space.function(dim=1, array=self.utmp)
        if (k == 2):
            F[:gdof] = M@utmp
        if (k == 4):
            
            @barycentric
            def f(bcs):
                val = utmp(bcs)
                return val**2
            uint = self.space.integralalg.mesh_integral(f)
            F[:gdof] = (uint+1)*M@utmp
        @barycentric
        def f(bcs):
            val = utmp(bcs)
            return -epsilon*val + s*val**2/2 - val**3/6
        F[:gdof] += dt*self.project(self.space.source_vector(f))
        return F

    def one_step_solve_semiimplict(self):
        """

        Notes
        -----
            求解一个时间层的数值解
        """

        gdof = self.space.number_of_global_dofs()
        A = self.get_current_left_matrix()
        F = self.get_current_right_vector()
        x = spsolve(A, F)
        self.uh0[:] = x[:gdof]
        self.uh1[:] = x[gdof:]
        
    def one_step_solve_APG(self, w, uh0, uh1, k, a=None):
        """

        Notes
        -----
            用APG求解一个时间层的数值解
            case1:  k=2
            case2:  k=4
        """
        gdof = self.space.number_of_global_dofs()
        A = self.get_current_left_matrix_APG(k, a)
        F = self.get_current_right_vector_APG(w, uh0, uh1, k)
        x = spsolve(A, F)
        self.uh0[:] = x[:gdof]
        self.uh1[:] = x[gdof:]
        
    def post_process(self):
        area = np.sum(self.space.cellmeasure)
        self.uh0 -= self.space.integralalg.mesh_integral(self.uh0)/area

    def Hamilton(self):
        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']
        uh0 = self.uh0
        uh1 = self.uh1

        @barycentric
        def f0(bcs):
            val0 = uh0(bcs)
            val1 = uh1(bcs)
            val = c*(val0 + val1)**2/2
            val += epsilon*val0**2/2
            val -= s*val0**3/6
            val += val0**4/24
            return val

        H = self.space.integralalg.mesh_integral(f0)/np.sum(self.space.cellmeasure)
        self.H.append(H)
        
        @barycentric
        def f1(bcs):
            val = uh0(bcs)
            return -s*val**2/2 + val**3/6
        grad = c*self.M*uh0 
        grad += epsilon*self.M*uh0
        grad -= c*2*self.A*uh0 
        grad += self.space.source_vector(f1) 
        grad -= c*self.A*uh1
        grad = self.project(grad)

        grad = self.space.function(dim=1, array=grad)

        @barycentric
        def f2(bcs):
            val = grad(bcs)
            return val**2
        grad = self.space.integralalg.mesh_integral(f2)

        self.G.append(grad)
       
    def solve_APG(self, disp=True, output=False, rdir='.', step=1,
            postprocess=False, k=2):
        """

        Notes
        -----

        计算所有的时间层。
        """
        self.one_step_solve_semiimplict()
        self.post_process()
        dt = self.dt
        uh0 = self.uh0.copy()
        uh1 = uh0
        current = 0
        theta0 = 1
        H = np.inf
        G = np.inf
        self.H.append(H)
        self.G.append(G)
        if (k==4):
            self.B = self.A*inv(M)*self.A
        while 1:
            print("timeline\n", current)
            theta1 = np.sqrt(theta0**2+0.25*theta0**4)-0.5*theta0**2
            w = theta0*(1-theta0)/(theta0**2+theta1)
            print("w\n", w)
            print("udiff", uh0)
            print("udiff", uh1)
            self.one_step_APG(w, uh0, uh1, k)

            v = self.uh0.copy()
            print("dt\n", self.dt)
        
            uh1 = self.space.function(dim=1,array=uh1)
            v = self.space.function(dim=1,array=v)
            
            @barycentric
            def f2(bcs):
                val0 = v(bcs)
                val1 = uh1(bcs)
                val = (val0 - val1)**2
                return val
            uDiff = self.space.integralalg.mesh_integral(f2)
            current += 1
            
            print("Ediff", self.H[-1]-self.H[-2])
            #if (self.H[-1]-self.H[-2] > -1e-12*uDiff):
            if (self.H[-1]-self.H[-2] > 0):
                print("restart", self.H[-1]-self.H[-2])
                theta0 = 1
                current -= 1
                del self.H[-1]
                del self.G[-1]
            else:
                theta0 = theta1
                uh0 = uh1.copy()
                uh1 = v.copy()
            
            if disp:
                print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
                print("Max phase value:", np.max(self.uh0))
                print("Min phase value:", np.min(self.uh0))

            if output & (current%step == 0):
                fname = rdir + '/step_'+ str(current).zfill(10) + '.vtu'

 
            print("Gdiff", self.G[-1]-self.G[-2])
            if (abs(self.H[-1] - self.H[-2]) < 1e-6):
                break
        return current


    def project(self, u):
        area = np.sum(self.space.cellmeasure)
        u = self.space.function(dim=1, array=u)
        
        @barycentric
        def f(bcs):
            val = u(bcs)
            return val

        u = u - self.space.integralalg.mesh_integral(f)/area
        return u
    
    def L2_norm(self, u):
        u = self.space.function(dim=1, array=u)

        @barycentric
        def f(bcs):
            val = u(bcs)
            return val

        f = self.space.integralalg.mesh_integral(f)
        return f

    
    def solve_PCG(self, G, g, M):
        """
        solve Gd=g, s.t. Ax=b,  dt?
        """
        i = 0
        gdof = self.gdof
        r = -g
        d = np.zeros(2*gdof)
        Minv = inv(M)

        Pr = self.project(r)
        p = -self.project(Minv@Pr)
        Pr = -p
        eta = 1e-4*min(1, self.L2_norm(g*g))
        error = 0
        while 1:
            Gp = G@p

            alpha = self.L2_norm(r*Pr)/self.L2_norm(p*Gp)
            
            d = d + alpha*p
            r1 = r + alpha*Gp

            Pr1 = self.project(r1)
            Pr1 = self.project(Minv@Pr1)

            beta = self.L2_norm(r1*Pr1)/self.L2_norm(r*Pr)

 
            p = -Pr1 + beta*p

            i = i+1
            r = r1
            Pr = Pr1
            error = np.sqrt(self.L2_norm(Pr*Pr))
            if (error < eta) and (i > 100):
                break
        return d
    
    def get_H(self, u):
        s = self.options['s']
        c = self.options['c']
        epsilon = self.options['epsilon']
        R = -self.A@u
        uh1 = spsolve(self.M, R)
        u = self.space.function(dim=1,array=u)
        uh1 = self.space.function(dim=1,array=uh1)

        @barycentric
        def f0(bcs):
            val0 = u(bcs)
            val1 = uh1(bcs)
            val = c*(val0 + val1)**2/2
            val += epsilon*val0**2/2
            val -= s*val0**3/6
            val += val0**4/24
            return val

        H =self.space.integralalg.mesh_integral(f0)/np.sum(self.space.cellmeasure)
        self.H.append(H)
        
        @barycentric
        def f1(bcs):
            val = u(bcs)
            return -s*val**2/2 + val**3/6
        grad = c*self.M*u
        grad += epsilon*self.M*u
        grad -= c*2*self.A*u
        grad += self.space.source_vector(f1) 
        grad -= c*self.A*uh1
        grad = self.project(grad)

        grad = self.space.function(dim=1, array=grad)

        @barycentric
        def f2(bcs):
            val = grad(bcs)
            return val**2
        grad = self.space.integralalg.mesh_integral(f2)

        self.G.append(grad)
 
    def solve_NewtonPCG(self, disp=True, output=False, rdir='.', step=1,
            postprocess=False, current = 0):
        """

        Notes
        -----

        计算所有的时间层。
        """


        current = current
        gdof = self.gdof
        print('uh', self.uh0)

        a = 10; #initial mu
        while 1:
            current +=1
            print("timeline\n", current)
            """
            use PCG to solve dk :G d =-g
            """
           ### G
            g1 = self.get_current_right_vector_PCG()
            g1[:gdof] = self.project(g1[:gdof])
            g = g1[:gdof]
            print('g',g)

            err = self.L2_norm(g**2)
            print('err', err)
            mu = max(1e-20, min(a*err,100))
           # mu = 0
            print('mu', mu)

            G = self.get_current_left_matrix_PCG(mu)
            ##precondition M

            M = self.get_Precondition(mu)
 
            d1 = self.solve_PCG(G, g1, M)
            #d1 = spsolve(G, g1)
            d =  self.project(d1[:gdof])
            ##update mu
            tmp = self.L2_norm(-g*d)

            rho = -tmp/self.L2_norm(d*d)
            print('rho', rho)

            if (rho > 0.1):
                a = 0.1*a
            elif (rho < 0.1):
                a = a*10
            else:
                a = 0.5*a

            t = 1
            i=0
            while 1:
                print('i',i)
                u = self.uh0 + t*d
                print('t',t)
                u = self.project(u)
                print('u', u)
                self.get_H(u)
                v = 1e-10
                inn = self.L2_norm(g*d)
                tol = v*t*inn
                print('tol',tol)
                print('Hdiff', self.H[-1]- self.H[-2])
                if ((self.H[-1]- self.H[-2]) <= tol):
                    break
                else:
                    print('linear')
                    t = 0.618*t
                    del self.H[-1]
                    del self.G[-1]
                i = i+1
            
            self.uh0 = u
            ###H

            if disp:
                print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
                print("Max phase value:", np.max(self.uh0))
                print("Min phase value:", np.min(self.uh0))

            if output & (current%step == 0):
                fname = rdir + '/step_'+ str(current).zfill(10) + '.vtu'
                print(fname)
                self.write_to_vtk(fname)
    

            print("Ediff", self.H[-1]-self.H[-2])
           
            if (abs(self.G[-1]) < 1e-06):
                print("end")
                break
    
    def solve_Hybrid(self, disp=True, output=False, rdir='.', step=1,
            postprocess=False, k=2):
        current = self.solve_APG(output=True, postprocess=True)
        print("Netown")
        self.solve_NewtonPCG(current=current)

    def solve(self, disp=True, output=False, rdir='.', step=1, postprocess=False):
        """

        Notes
        -----

        计算所有的时间层。
        """
        dt = self.dt
        current = 0
        self.post_process()
        self.H.append(np.inf)
        self.G.append(np.inf)
        print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
        while 1:
            self.one_step_solve_semiimplict()
            if postprocess:
                self.post_process()
            self.Hamilton()
            current += 1

            if disp:
                print("Current Hamilton energy ", self.H[-1], " with gradient ",
                        self.G[-1])
                print("Max phase value:", np.max(self.uh0))
                print("Min phase value:", np.min(self.uh0))

            if output & (current%step == 0):
                fname = rdir + '/step_'+ str(current).zfill(10) + '.vtu'
                print(fname)
                self.write_to_vtk(fname)
                print('gdiff', abs(self.G[-1]-self.G[-2])) 
                print('Hdiff', self.H[-1]-self.H[-2])
            if (self.H[-1] > self.H[-2]):
                break
            if (abs(self.G[-1]-self.G[-2]) < 1e-8) and (abs(self.H[-1] -self.H[-2]) <
                    1e-8):
                print('gdiff', abs(self.G[-1]-self.G[-2])) 
                break

    def write_to_vtk(self, fname):
        self.mesh.nodedata['uh0'] = self.uh0
        self.mesh.nodedata['uh1'] = self.uh1
        self.mesh.to_vtk(fname=fname)

