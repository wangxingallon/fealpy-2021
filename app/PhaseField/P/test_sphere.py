import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as a3
import sys
from matplotlib import cm
from fealpy.geometry import SphereSurface
from fealpy.timeintegratoralg import UniformTimeLine
from fealpy.mesh.LagrangeTriangleMesh import LagrangeTriangleMesh
#from fealpy.mesh.TriangleMesh import TriangleMesh
from PhaseFieldCrystalModel import PhaseFieldCrystalModel
#from fealpy.fem import PhaseFieldCrystalModel

n = int(sys.argv[1])#网格加密次数
p = int(sys.argv[2])#基函数次数
f = int(sys.argv[3])#初值选取
R = float(sys.argv[4])#半径大小


options1 = {'c': 1, 's': 0.3, 'epsilon': -0.13, 'order':p}
options2 = {'c': 1, 's': 0, 'epsilon': -1, 'order':p}
options3 = {'c': 0.001, 's': 0.2, 'epsilon': -0.3, 'order':p}

if f==1 or f==2:
    options = options3
else:
    options = options2

surface = SphereSurface(radius=R)
mesh = surface.init_mesh(p=p)
mesh.uniform_refine(n=n)

model = PhaseFieldCrystalModel(mesh, options)

#初值选取
def u(fieldstype=1):
    u = model.space.function()

    if fieldstype == 1:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5* np.cos(np.pi*p[..., 0])*np.cos(np.pi*p[..., 1])*np.cos(np.pi*p[..., 2]))
    elif fieldstype == 2:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5*np.cos(p[..., 0])*np.cos(p[...,1])*np.cos(p[..., 2]))

    elif fieldstype == 3:
        u[:] = model.space.interpolation(lambda p: 0.5 + 0.5*np.cos(np.pi*p[..., 0]))
    elif fieldstype == 4:
        u[:] = model.space.interpolation(lambda p: np.cos(5*p[..., 0]))
    return u


print("gdof", model.gdof)
data = model.set_init_solution(u(fieldstype=f))
model.solve_Hybrid(output=True, postprocess=True)
#model.solve_APG(output=True, postprocess=True, k=4)
#model.solve(output=True, postprocess=True)
H1 = model.H
uh = model.uh0
G1 = model.G
import scipy.io as sio

data = {
        'H':H1,
        'G':G1,
        'uh':uh
        }
sio.savemat('rsts.mat', data)

np.save('G1',G1)
np.save('H1',H1)

fig = plt.figure()
axes = a3.Axes3D(fig)
mesh.add_plot(axes, linewidths=0.5)
plt.show()

N = len(H1)
x = np.linspace(0,N,N)

fig3 = plt.figure(3)
axes3 = fig3.add_subplot(2,1,1)
plt.plot(x,model.H, 'g--')
plt.title('Hamilton')
axes4 = fig3.add_subplot(2,1,2)
plt.plot(x,model.G,'r--')
plt.title('grad')

plt.show()

