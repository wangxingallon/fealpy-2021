%%%
clear; clc; close all;

%%% 'on': 图像及时显示; 'off': 取消及时显示;
set(0, 'DefaultFigureVisible', 'on');

%%% 创建文件夹存储图像;
if exist('Figures/') == 0
    mkdir('Figures/');
end

load('./P/rst.mat');
G1=log10(G);
x=1:1:length(G1);

load('./P/rst1.mat');
G2=log10(G);
y=1:1:length(G2);
%%% 基本设置;
figure('Name', 'Draw Requirements', 'NumberTitle', 'off', 'Position', [400,20,800,800]);
set(gca, 'LineWidth', 2, 'FontName', 'Times New Roman', 'FontSize', 25);
hold on;
box on;
axis tight;

f1=plot(x, G1, ':', 'LineWidth', 2, 'Color', [255 69 0]/255, 'MarkerSize', 12, 'Marker', 'none',...
    'MarkerFaceColor', 'none', 'MarkerEdgeColor', [238 0 0]/255);
f2=plot(y, G2, '-', 'LineWidth', 2, 'Color', [0 0 255]/255, 'MarkerSize', 12, 'Marker', 'none',...
    'MarkerFaceColor', 'none', 'MarkerEdgeColor', [0 0 255]/255);
ylabel('grad', 'Interpreter', 'Latex', 'FontName', 'Times New Roman', 'FontSize', 25, 'Rotation', pi/2);
% ylim([-0.4, 3]);
% ax = gca; ax.YAxis(1).Exponent = 2;
set(gca, 'YTick', [-6:2:0]);
set(gca, 'YTickLabel', {'10^{-6}', '10^{-4}', '10^{-2}','10^{0}'});
ylim([-4.8,-1.5]);
xlim([-1,177])
xlabel('Iter', 'Interpreter', 'Latex', 'FontName', 'Times New Roman', 'FontSize', 25);
%%% 手动设置图例; 通过句柄可以选择哪些显示;
legend([f1,f2],{'SIS','AA-BPG2'}, 'Location', 'northeast',...
  	 'Interpreter', 'Latex', 'FontName', 'Times New Roman', 'FontSize', 25);
legend('boxon');
%%% 保存图片, 请自行选择图片格式;
FigName = 'Figures/G';
saveas(gcf, FigName, 'svg'); % svg, psc2, pdf, png, jpeg;
saveas(gcf, FigName, 'eps'); % svg, psc2, pdf, png, jpeg;
saveas(gcf, FigName, 'pdf'); % svg, psc2, pdf, png, jpeg;